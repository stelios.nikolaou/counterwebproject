package com.qaagility.controller;

import org.junit.Test;
import static org.junit.Assert.*;

public class CntTest {

  @Test
  public void dTest0() throws Exception {
    assertEquals("d",Integer.MAX_VALUE, new Cnt().d(100,0));
  }

  @Test
  public void dTest1() throws Exception {
    assertEquals("d",20, new Cnt().d(100,5));
  }

}
