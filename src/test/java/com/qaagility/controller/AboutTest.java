package com.qaagility.controller;

import org.junit.Test;
import static org.junit.Assert.*;

public class AboutTest {

  @Test
  public void testDesc() throws Exception {
    assertTrue("Desc", new About().desc().contains("police!"));
  }

}
