package com.qaagility.controller;

import org.junit.Test;
import static org.junit.Assert.*;

public class CalcDevideTest {

  @Test
  public void devideTest0() throws Exception {
    assertEquals("Devide",Integer.MAX_VALUE, new CalcDevide().devide(100,0));
  }

  @Test
  public void devideTest1() throws Exception {
    assertEquals("Devide",20, new CalcDevide().devide(100,5));
  }

}
